#include <stdio.h>
#include <stdlib.h>
#include <math.h>
int main(int argc, char const *argv[]) {
  float x1,x2,y1,y2,d;
  printf("\n Caluladora de distancias entre puntos\n Ingrese coordenadas de p1 en el eje cartesiano: ");
  scanf("%f,%f",&x1,&y1);
  printf(" Ingrese coordenadas de p2: ");
  scanf("%f,%f",&x2,&y2);
  d = sqrt(pow(abs(x2-x1),2)+pow(abs(y2-y1),2));
  printf(" La distancia entre p1(%.2f,%.2f) y p2(%.2f,%.2f) es %.3f\n\n",x1,y1,x2,y2,d);
  system("pause");
  return 0;
}
