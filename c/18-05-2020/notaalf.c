#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(int argc, char const *argv[]){
  char nota,cat[12];
  printf(" Ingrese la calificacion alfabetica: ");
  scanf("%c",&nota);
  switch(nota){
    case 65: case 97:  strcpy(cat,"Excelente"); break;
    case 66: case 98:  strcpy(cat,"Buena"); break;
    case 67: case 99:  strcpy(cat,"Regular"); break;
    case 68: case 100: strcpy(cat,"Suficiente"); break;
    case 69: case 101: strcpy(cat,"Insuficiente"); break;
    default: strcpy(cat,"Undefined");
  }
  printf(" Su nota es %s.\n\n", cat);
  system("pause");
  return 0;
}
